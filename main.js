
var tl;
var loops = 0;


function init() {
	tl = new TimelineMax();

	
	var time = 0.5;
	var between = 0.25;
	var wait = 2;




	
	tl.from("#terbang", time, {autoAlpha:0,x:"-50"});

	tl.from("#copy1", time, {autoAlpha:0},"-="+between);
	tl.to("#terbang", time, {autoAlpha:0,x:"50"},"-=0.5");
	tl.from("#masker", time, {autoAlpha:0,width:"0px"},"-=0.3");

	
	tl.to("#copy1", time, {autoAlpha:0},"+="+1);
	tl.to("#masker", time, {autoAlpha:0,width:"299px"},"+="+1);

//-------------------------------------------------

	tl.from("#frame2-circle", time, {autoAlpha:0,scale:1.8},"-=2");
	tl.from("#frame2-hero", time, {autoAlpha:0, scale:0.5},"-=1.75");
	tl.from("#frame2-copy2", time, {autoAlpha:0, y:"+50"},"-=1.5");

	tl.to("#frame2-circle", time, {delay:wait, autoAlpha:0});
	tl.to("#frame2-hero", time, {autoAlpha:0},"-=0.5");
	tl.to("#frame2-copy2", time, {autoAlpha:0},"-=0.5");


	
}




var e = getElement = function(name) {
	return document.getElementById(name);
};